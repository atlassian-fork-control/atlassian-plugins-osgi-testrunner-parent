package com.atlassian.plugins.osgi.test;

import com.atlassian.plugins.osgi.test.rest.GsonFactory;
import com.google.gson.Gson;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.lang.annotation.Annotation;

import static org.junit.Assert.assertNotNull;

/**
 * @since version
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class TestResultSerialization
{

    private final Gson gson = GsonFactory.getGson();

    @Test
    public void resultSerializesWithGson() throws Exception
    {
        Annotation[] annos = getClass().getAnnotations();
        Result result = new Result();
        RunListener listener = result.createListener();
        Description description = Description.createTestDescription(TestResultSerialization.class,TestResultSerialization.class.getName(),annos);
        listener.testRunStarted(description);
        ComparisonFailure exception = new ComparisonFailure("failed test","expected this","got that");
        exception.getStackTrace();

        Failure failure = new Failure(description,exception);

        listener.testFailure(failure);
        listener.testFinished(description);

        String json = gson.toJson(result);

        assertNotNull(json);
    }

    @Test
    public void resultDeserializesWithGson() throws Exception
    {

        Annotation[] annos = getClass().getAnnotations();
        Result result = new Result();
        RunListener listener = result.createListener();
        Description description = Description.createTestDescription(TestResultSerialization.class,TestResultSerialization.class.getName(),annos);
        listener.testRunStarted(description);
        ComparisonFailure exception = new ComparisonFailure("failed test","expected this","got that");
        exception.getStackTrace();

        Failure failure = new Failure(description,exception);
        listener.testFailure(failure);
        listener.testFinished(description);

        String json = gson.toJson(result);

        Result osgiResult = gson.fromJson(json,Result.class);

        assertNotNull(osgiResult);
    }

    
}
