package com.atlassian.plugins.osgi.test.rest;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class BundleResultRepresentation
{
    @JsonProperty private final String bundleSymbolicName;
    @JsonProperty private final TestResultDetailRepresentation testResultDetail;

    @JsonCreator
    public BundleResultRepresentation(@JsonProperty("bundleSymbolicName") String bundleSymbolicName, @JsonProperty("testResultDetail") TestResultDetailRepresentation testResultDetail)
    {
        this.bundleSymbolicName = bundleSymbolicName;
        this.testResultDetail = testResultDetail;
    }

    public String getBundleSymbolicName()
    {
        return bundleSymbolicName;
    }

    public TestResultDetailRepresentation getTestResultDetail()
    {
        return testResultDetail;
    }
}
